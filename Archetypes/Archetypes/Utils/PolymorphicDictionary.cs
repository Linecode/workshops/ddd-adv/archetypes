namespace Archetypes.Utils;

public class PolymorphicDictionary<TValue> where TValue : notnull
{
    private readonly IDictionary<Type, TValue?> _inner = new Dictionary<Type, TValue?>();
  
    public bool ContainsKey(Type key)
    {
        return FindEntry(key).Value is not null;
    }

    public TValue? this[Type key]
    {
        get => FindEntry(key).Value;
        set => _inner[key] = value;
    }

    private KeyValuePair<Type, TValue?> FindEntry(Type key)
    {
        return _inner.FirstOrDefault(kvp => key.IsAssignableFrom(kvp.Key));
    }
}