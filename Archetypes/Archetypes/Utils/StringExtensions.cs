namespace Archetypes.Utils;

public static class StringExtensions
{
    public static bool IsNotNullNorWhiteSpace(this string value)
        => !string.IsNullOrWhiteSpace(value);
}