namespace Archetypes.Party;

public record PartyId
{
    private readonly Guid _id;

    public PartyId()
    {
        _id = Guid.NewGuid();
    }
    
    public PartyId(Guid id)
    {
        _id = id;
    }

    public Guid ToGuid() => _id;
}