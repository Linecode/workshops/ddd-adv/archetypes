using System.Text.RegularExpressions;

namespace Archetypes.Party.Address;

public class WebPageAddress : Address
{
    public string Url { get; private set; }

    private const string Pattern = @"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$";
    
    private WebPageAddress(string url)
    {
        Url = url;
    }

    public static WebPageAddress Create(string url)
    {
        var regex = new Regex(Pattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);
        
        if (regex.IsMatch(url))
            return new WebPageAddress(url);
        
        throw new ArgumentException("Invalid URL", nameof(url));
    }
    
    public override string GetAddress()
    {
        return Url;
    }
}