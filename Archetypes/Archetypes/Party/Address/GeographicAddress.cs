using System.Text;
using Archetypes.Utils;

namespace Archetypes.Party.Address;

public class GeographicAddress : Address
{
    private readonly List<string> _addressLines;
    public IReadOnlyList<string> AddressLines => _addressLines;
    public string City { get; private set; }
    public string RegionOrState { get; private set; }
    public string ZipOrPostCode { get; private set; }

    private GeographicAddress(IList<string> addressLines, string city, string regionOrState, string zipOrPostCode)
    {
        _addressLines = new List<string>(addressLines);
        City = city;
        RegionOrState = regionOrState;
        ZipOrPostCode = zipOrPostCode;
    }

    public static GeographicAddress Create(
        IList<string> addressLines, 
        string city, 
        string reginOrState,
        string zipOrPostCde)
    {
        // Do some validation
        
        return new GeographicAddress(addressLines, city, reginOrState, zipOrPostCde);
    }

    public override string GetAddress()
    {
        var sb = new StringBuilder();
        
        foreach (var line in _addressLines)
        {
            sb.AppendLine(line);
        }
        
        sb.Append(ZipOrPostCode);
        
        if (RegionOrState.IsNotNullNorWhiteSpace())
            sb.Append($", {RegionOrState}");
        
        sb.Append($", {City}");
        
        return sb.ToString();
    }
}