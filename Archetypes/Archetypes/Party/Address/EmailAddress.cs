using System.Globalization;
using System.Text.RegularExpressions;

namespace Archetypes.Party.Address;

public class EmailAddress : Address
{
    public string Email { get; private set; }

    private EmailAddress(string email)
    {
        Email = email;
    }

    public static EmailAddress Create(string email)
        => IsValidEmail(email)
            ? new EmailAddress(email)
            : throw new ArgumentException("Invalid email address", nameof(email));

    private static bool IsValidEmail(string email)
    {
        if (string.IsNullOrWhiteSpace(email))
            return false;

        try
        {
            email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                RegexOptions.None, TimeSpan.FromMilliseconds(200));

            string DomainMapper(Match match)
            {
                var idn = new IdnMapping();

                var domainName = idn.GetAscii(match.Groups[2].Value);

                return match.Groups[1].Value + domainName;
            }
        }
        catch (RegexMatchTimeoutException e)
        {
            return false;
        }
        catch (ArgumentException e)
        {
            return false;
        }

        try
        {
            return Regex.IsMatch(email,
                @"^[^@\s]+@[^@\s]+\.[^@\s]+$",
                RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
        }
        catch (RegexMatchTimeoutException)
        {
            return false;
        }
    }

    public override string GetAddress()
    {
        return Email;
    }
}