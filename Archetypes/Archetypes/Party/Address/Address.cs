namespace Archetypes.Party.Address;

// Address archetype
public abstract class Address
{
    public abstract string GetAddress();
}