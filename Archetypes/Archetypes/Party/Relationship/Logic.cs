using Archetypes.Party.Relationship.Role;
using Archetypes.Party.Utils;

namespace Archetypes.Party.Relationship;

public static class Logic
{
    public static void Run()
    {
        var personOnB2C = PersonMotherObject.GetRandomPerson();
        var personOnPerm = PersonMotherObject.GetRandomPerson();
        var organization = OrganizationMotherObject.GetRandomOrganization();

        var repository = new InMemoryPartyRelationshipRepository();
        
        // Saving the relationship
        repository.Save(
            PartyRelationshipDictionary.Employment.ToString(),
            PartyRolesDictionary.B2CEmployee.ToString(), personOnB2C,
            PartyRolesDictionary.Employer.ToString(), organization
                );
        repository.Save(
            PartyRelationshipDictionary.Employment.ToString(),
            PartyRolesDictionary.PermEmployee.ToString(), personOnPerm,
            PartyRolesDictionary.Employer.ToString(), organization
        );
        
        // Retriving the relationship for b2c employee
        var relation = repository.FindRelationshipFor(personOnB2C.Id, PartyRelationshipDictionary.Employment.ToString());

        if (relation is not null)
        {
            var role = RoleObjectFactory.From(relation).GetRole<RoleForEmployment>();
            
            role.Sign();
        }
        
        // Retriving the relationship for perm employee
        relation = repository.FindRelationshipFor(personOnPerm.Id, PartyRelationshipDictionary.Employment.ToString());

        if (relation is not null)
        {
            var role = RoleObjectFactory.From(relation).GetRole<RoleForEmployment>();
            
            role.Sign();
        }
    }
}