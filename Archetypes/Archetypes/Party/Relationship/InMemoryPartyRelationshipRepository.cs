namespace Archetypes.Party.Relationship;

public class InMemoryPartyRelationshipRepository : List<PartyRelationship>
{
    public PartyRelationship Save(string partyRelationship,
        string partyARole,
        Party partyA,
        string partyBRole,
        Party partyB)
    {
        PartyRelationship relationship;
        var parties = this
            .Where(x => x.Name == partyRelationship &&
                        ((x.PartyA.Id == partyA.Id && x.PartyB.Id == partyB.Id) ||
                         (x.PartyA.Id == partyB.Id && x.PartyB.Id == partyA.Id)))
            .ToList();

        if (!parties.Any())
        {
            relationship = new PartyRelationship();
            Add(relationship);
        }
        else
        {
            relationship = parties.First();
        }
        
        relationship.Name = partyRelationship;
        relationship.PartyA = partyA;
        relationship.PartyB = partyB;
        relationship.RoleA = partyARole;
        relationship.RoleB = partyBRole;

        return relationship;
    }

    public PartyRelationship? FindRelationshipFor(PartyId id, string relationshipName)
    {
        var parties = this.Where(x =>
            x.Name == relationshipName && (x.PartyA.Id == id || x.PartyB.Id == id)).ToList();
        
        return parties.FirstOrDefault();
    }
}