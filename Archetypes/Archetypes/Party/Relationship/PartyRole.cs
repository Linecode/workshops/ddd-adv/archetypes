namespace Archetypes.Party.Relationship;

public class PartyRole
{
    public Guid? Id { get; set; }
    public string Name { get; set; }
}