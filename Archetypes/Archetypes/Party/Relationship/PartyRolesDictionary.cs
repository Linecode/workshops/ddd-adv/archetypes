using Archetypes.Party.Relationship.Role;

namespace Archetypes.Party.Relationship;

public class PartyRoleNameAttribute : Attribute
{
    public string Name { get; }
    
    public PartyRoleNameAttribute(string name)
    {
        Name = name;
    }
}

public static class PartyRolesDictionary
{
    public static PartyRolesDictionary<B2CEmployee> B2CEmployee => new(nameof(B2CEmployee));
    public static PartyRolesDictionary<PermEmployee> PermEmployee => new(nameof(PermEmployee));
    public static PartyRolesDictionary<Employer> Employer => new(nameof(Employer));
}

public sealed record PartyRolesDictionary<T> where T : PartyBasedRole
{
    private readonly string _memberName;

    public PartyRolesDictionary(string memberName)
    {
        _memberName = memberName;
    }

    public string RoleName => typeof(T).Name;

    public override string ToString()
    {
        return _memberName;
    }
}