namespace Archetypes.Party.Relationship.Role;

public class Employer : PartyBasedRole
{
    public Employer(Party party) : base(party)
    {
    }
}