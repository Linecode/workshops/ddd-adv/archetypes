namespace Archetypes.Party.Relationship.Role;

public abstract class PartyBasedRole
{
    protected Party Party;
    
    public PartyBasedRole(Party party)
    {
        Party = party;
    }
}