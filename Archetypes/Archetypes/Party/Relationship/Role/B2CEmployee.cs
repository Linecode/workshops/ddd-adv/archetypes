namespace Archetypes.Party.Relationship.Role;

public class B2CEmployee : RoleForEmployment
{
    public B2CEmployee(Party party) : base(party)
    {
    }

    public override void Sign()
    {
        Console.WriteLine("B2C Employee signed");
    }
}