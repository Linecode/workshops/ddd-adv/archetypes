namespace Archetypes.Party.Relationship.Role;

public abstract class RoleForEmployment : PartyBasedRole
{
    public RoleForEmployment(Party party) : base(party)
    {
    }

    public abstract void Sign();
}