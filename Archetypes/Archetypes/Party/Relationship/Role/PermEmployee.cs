namespace Archetypes.Party.Relationship.Role;

public class PermEmployee : RoleForEmployment
{
    public PermEmployee(Party party) : base(party)
    {
    }

    public override void Sign()
    {
        Console.WriteLine("Perm Employee signed");
    }
}