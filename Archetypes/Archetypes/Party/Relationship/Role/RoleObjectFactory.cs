using Archetypes.Utils;

namespace Archetypes.Party.Relationship.Role;

public class RoleObjectFactory
{
    private readonly PolymorphicDictionary<PartyBasedRole> _roles = new();

    public bool HasRole<T>() where T : PartyBasedRole
    {
        return _roles.ContainsKey(typeof(T));
    }

    public static RoleObjectFactory From(PartyRelationship partyRelationship)
    {
        var roleObject = new RoleObjectFactory();
        roleObject.Add(partyRelationship);
        return roleObject;
    }

    private void Add(PartyRelationship partyRelationship)
    {
        Add(partyRelationship.RoleA, partyRelationship.PartyA);
        Add(partyRelationship.RoleB, partyRelationship.PartyB);
    }

    private readonly Dictionary<string, Type> _map = new()
    {
        { "PermEmployee", typeof(PermEmployee) },
        { "B2CEmployee", typeof(B2CEmployee) },
        { "Employer", typeof(Employer) },
    };

    private void Add(string role, Party party)
    {
        try
        {
            // very simple mapper
            if (!_map.TryGetValue(role, out var type))
                throw new Exception($"Unknown role: {role}");

            var instance = (PartyBasedRole)Activator.CreateInstance(type, party);
            _roles[type] = instance;
        }
        catch (Exception e)
        {
            throw new ArgumentException("invalid role", nameof(role), e);
        }
    }

    public T? GetRole<T>() where T : PartyBasedRole
    {
        return _roles[typeof(T)] as T;
    }
}