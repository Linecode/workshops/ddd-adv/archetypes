using Archetypes.Party.Address;
using Archetypes.Party.Parties;
using Bogus;
using Person = Archetypes.Party.Parties.Person;

namespace Archetypes.Party.Utils;

public static class PersonMotherObject
{
    public static Person GetRandomPerson()
    {
        var faker = new Faker();
        
        return Person.Create(
            faker.Person.FirstName,
            faker.Person.LastName,
            faker.Random.Number(100000000, 999999999).ToString(),
            EmailAddress.Create(faker.Person.Email),
            GeographicAddress.Create(new [] {faker.Address.StreetAddress()}, faker.Address.City(), faker.Address.State(), faker.Address.ZipCode()),
            faker.PickRandom<ISOGender>()
        );
    }
}