using Archetypes.Party.Address;
using Archetypes.Party.Parties;
using Bogus;

namespace Archetypes.Party.Utils;

public static class OrganizationMotherObject
{
    public static Organization GetRandomOrganization()
    {
        var faker = new Faker();

        return Organization.Create(
            faker.Company.CompanyName(),
            faker.Random.Number(100000000, 999999999).ToString(),
            WebPageAddress.Create(faker.Internet.Url()),
            EmailAddress.Create(faker.Person.Email),
            GeographicAddress.Create(new[] { faker.Address.StreetAddress() }, faker.Address.City(),
                faker.Address.State(), faker.Address.ZipCode()));
    }
}