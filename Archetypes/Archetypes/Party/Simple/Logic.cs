using Archetypes.Party.Utils;

namespace Archetypes.Party.Simple;

public static class Logic
{
    public static void Run()
    {
        var parties = new List<Party>
        {
            PersonMotherObject.GetRandomPerson(),
            OrganizationMotherObject.GetRandomOrganization(),
            PersonMotherObject.GetRandomPerson(),
            OrganizationMotherObject.GetRandomOrganization()
        };

        foreach (var party in parties)
        {
            Console.WriteLine($"{party.GetType().Name} Contact Information:");
            Console.WriteLine(party.GetContactInformation());
            Console.WriteLine("-------------------------------------");
        }
    }
}