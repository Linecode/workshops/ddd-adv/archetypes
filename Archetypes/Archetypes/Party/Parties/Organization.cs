using Archetypes.Party.Address;

namespace Archetypes.Party.Parties;

public class Organization : Party
{
    public string Name { get; protected set; }
    public string TaxId { get; protected set; }
    
    public WebPageAddress WebPage { get; protected set; }
    public EmailAddress Email { get; protected set; }
    public GeographicAddress Address { get; protected set; }
    
    private List<OrganizationUnit> _organizationUnits = new();
    public IReadOnlyList<OrganizationUnit> OrganizationUnits => _organizationUnits.AsReadOnly();

    protected Organization(string name, string taxId, WebPageAddress webPage, EmailAddress email,
        GeographicAddress address)
    {
        Name = name;
        TaxId = taxId;
        WebPage = webPage;
        Email = email;
        Address = address;
    }

    public static Organization Create(string name, string taxId, WebPageAddress webPage, EmailAddress email,
        GeographicAddress address)
    {
        // Do some validation
        
        var organization = new Organization(name, taxId, webPage, email, address)
        {
            Id = new PartyId()
        };
        return organization;
    }

    public static Organization Create(PartyId id, string name, string taxId, WebPageAddress webPage, EmailAddress email,
        GeographicAddress address)
    {
        // Do some validation
        
        var organization = new Organization(name, taxId, webPage, email, address)
        {
            Id = id
        };
        return organization;
    }

    public override string GetContactInformation()
        => Address.GetAddress();
}

// OrganizationUnit Archetype

public abstract class OrganizationUnit
{
    
}

public class Division : OrganizationUnit
{
    
}

public class Department : OrganizationUnit
{
    
}

// In Archetype it is called Team but maybe in our company it is called Squad
public class Squad : OrganizationUnit
{
    
}

// IT can goes further
// ... if needed, be careful to not overextend this
public abstract class Company : Organization
{
    protected Company(string name, string taxId, WebPageAddress webPage, EmailAddress email, GeographicAddress address) : base(name, taxId, webPage, email, address)
    {
    }
}

// ReSharper disable once InconsistentNaming
public abstract class USBasedCompany : Company
{
    protected USBasedCompany(string name, string taxId, WebPageAddress webPage, EmailAddress email, GeographicAddress address) : base(name, taxId, webPage, email, address)
    {
    }
}

public class GeneralCorporation : USBasedCompany
{
    protected GeneralCorporation(string name, string taxId, WebPageAddress webPage, EmailAddress email, GeographicAddress address) : base(name, taxId, webPage, email, address)
    {
    }
}

public class ClosedCorporation : USBasedCompany
{
    protected ClosedCorporation(string name, string taxId, WebPageAddress webPage, EmailAddress email, GeographicAddress address) : base(name, taxId, webPage, email, address)
    {
    }
}

public class SubchapterSCorporation : USBasedCompany
{
    protected SubchapterSCorporation(string name, string taxId, WebPageAddress webPage, EmailAddress email, GeographicAddress address) : base(name, taxId, webPage, email, address)
    {
    }
}

public class LimitedLiabilityCompany : USBasedCompany
{
    protected LimitedLiabilityCompany(string name, string taxId, WebPageAddress webPage, EmailAddress email, GeographicAddress address) : base(name, taxId, webPage, email, address)
    {
    }
}

// ReSharper disable once InconsistentNaming
public abstract class PLBasedCompany : Company
{
    protected PLBasedCompany(string name, string taxId, WebPageAddress webPage, EmailAddress email, GeographicAddress address) : base(name, taxId, webPage, email, address)
    {
    }
}

public class SoleProprietorship : PLBasedCompany
{
    public SoleProprietorship(string name, string taxId, WebPageAddress webPage, EmailAddress email, GeographicAddress address) : base(name, taxId, webPage, email, address)
    {
    }
}

public class PublicCompany : PLBasedCompany
{
    public PublicCompany(string name, string taxId, WebPageAddress webPage, EmailAddress email, GeographicAddress address) : base(name, taxId, webPage, email, address)
    {
    }
}