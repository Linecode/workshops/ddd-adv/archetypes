using Archetypes.Party.Address;

namespace Archetypes.Party.Parties;

public class Person : Party
{
    // FirstName and LastName can we wrapped into PersonName archetype
    public string FirstName { get; private set; }
    public string LastName { get; private set; }
    // ReSharper disable once InconsistentNaming
    public string SSN { get; private set; }
    public EmailAddress Email { get; private set; }
    public GeographicAddress Address { get; private set; }
    public ISOGender Gender { get; private set; }

    private Person(string firstName, string lastName, string ssn, EmailAddress email, GeographicAddress address, ISOGender gender)
    {
        FirstName = firstName;
        LastName = lastName;
        SSN = ssn;
        Email = email;
        Address = address;
        Gender = gender;
    }

    public static Person Create(string firstName, string lastName, string ssn, EmailAddress email,
        GeographicAddress address, ISOGender gender)
    {
        // Do some validation
        
        var person = new Person(firstName, lastName, ssn, email, address, gender)
        {
            Id = new PartyId()
        };
        return person;
    }
    
    public static Person Create(PartyId id,string firstName, string lastName, string ssn, EmailAddress email,
        GeographicAddress address, ISOGender gender)
    {
        // Do some validation
        
        var person = new Person(firstName, lastName, ssn, email, address, gender)
        {
            Id = id
        };
        return person;
    }

    public override string GetContactInformation()
        => Email.GetAddress();
}

// ReSharper disable once InconsistentNaming
public enum ISOGender
{
    Male,
    Female,
    NotKnown,
    NotSpecified
}