namespace Archetypes.Party;

// Party archetype
public abstract class Party
{
    public PartyId Id { get; protected set; } = null!;

    // Archetype extended with common behavior
    public abstract string GetContactInformation();
}