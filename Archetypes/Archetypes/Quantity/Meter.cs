namespace Archetypes.Quantity;

public class Meter : Unit
{
    public override double ConvertToBaseUnit(double value)
    {
        return value; // Meter is considered the base unit for length
    }

    public override double ConvertFromBaseUnit(double value)
    {
        return value; // Meter is considered the base unit for length
    }
}