namespace Archetypes.Quantity;

public abstract class Unit
{
    public abstract double ConvertToBaseUnit(double value);
    public abstract double ConvertFromBaseUnit(double value);
}