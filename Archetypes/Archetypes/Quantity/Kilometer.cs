namespace Archetypes.Quantity;

public class Kilometer : Unit
{
    public override double ConvertToBaseUnit(double value)
    {
        return value * 1000; // 1 Kilometer = 1000 Meters
    }

    public override double ConvertFromBaseUnit(double value)
    {
        return value / 1000; // 1 Meter = 0.001 Kilometers
    }
}