namespace Archetypes.Quantity.RoundingPolicies;

public sealed class RoundUpByStepPolicy : IRoundPolicy
{
    public decimal RoundingStep { get; }
    public RoundUpByStepPolicy(decimal roundingStep) => RoundingStep = roundingStep;

    public decimal Round(decimal amount)
    {
        int amountSign = Math.Sign(amount);
        decimal absoluteAmount = Math.Abs(amount);

        bool isThereReminder = decimal.Remainder(absoluteAmount, RoundingStep) != 0;
        decimal multiplicator = isThereReminder ? absoluteAmount / RoundingStep + 1 : absoluteAmount / RoundingStep;

        return amountSign switch
        {
            1 => decimal.Floor(multiplicator) * RoundingStep,
            0 => 0,
            -1 => decimal.Floor(multiplicator) * RoundingStep * -1,
            _ => throw new ArgumentOutOfRangeException()
        };
    }
}