namespace Archetypes.Quantity.RoundingPolicies;

public interface IRoundPolicy
{
    public decimal Round(decimal amount);
}