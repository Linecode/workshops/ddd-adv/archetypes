using Ardalis.SmartEnum;

namespace Archetypes.Quantity.RoundingPolicies;

public sealed class RoundDownPolicy : IRoundPolicy
{
    public byte Precision { get; }
    public RoundDownPolicy(byte precision) => Precision = precision;

    public decimal Round(decimal amount)
        => amount switch
        {
            > decimal.Zero => Math.Round(amount, Precision, MidpointRounding.ToNegativeInfinity),
            < decimal.Zero => Math.Round(amount, Precision, MidpointRounding.ToPositiveInfinity),
            _ => amount
        };
}

public class RoundingDigit : SmartEnum<RoundingDigit>
{
    public static readonly RoundingDigit Zero = new("0", 0);
    public static readonly RoundingDigit One = new("1", 1);
    public static readonly RoundingDigit Two = new("2", 2);
    public static readonly RoundingDigit Three = new("3", 3);
    public static readonly RoundingDigit Four = new("4", 4);
    public static readonly RoundingDigit Five = new("5", 5);
    public static readonly RoundingDigit Six = new("6", 6);
    public static readonly RoundingDigit Seven = new("7", 7);
    public static readonly RoundingDigit Eight = new("8", 8);
    public static readonly RoundingDigit Nine = new("9", 9);

    private RoundingDigit(string name, int value) : base(name, value) { }
}
