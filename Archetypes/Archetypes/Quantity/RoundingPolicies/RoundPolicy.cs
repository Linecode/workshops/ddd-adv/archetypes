namespace Archetypes.Quantity.RoundingPolicies;

public sealed class RoundPolicy : IRoundPolicy
{
    public RoundingDigit RoundingDigit { get; }
    public byte Precision { get; }

    public RoundPolicy(RoundingDigit roundingDigit, byte precision)
    {
        RoundingDigit = roundingDigit;
        Precision = precision;
    }

    public decimal Round(decimal amount)
    {
        decimal lastDigit = Math.Abs(amount) * new decimal(Math.Pow(10, Precision + 1)) % 10;

        return lastDigit >= RoundingDigit.Value ? RoundUp(amount) : RoundDown(amount);
    }

    private decimal RoundUp(decimal amount)
        => amount switch
        {
            > decimal.Zero => Math.Round(amount, Precision, MidpointRounding.ToPositiveInfinity),
            < decimal.Zero => Math.Round(amount, Precision, MidpointRounding.ToNegativeInfinity),
            _ => amount
        };

    private decimal RoundDown(decimal amount)
        => amount switch
        {
            > decimal.Zero => Math.Round(amount, Precision, MidpointRounding.ToNegativeInfinity),
            < decimal.Zero => Math.Round(amount, Precision, MidpointRounding.ToPositiveInfinity),
            _ => amount
        };
}