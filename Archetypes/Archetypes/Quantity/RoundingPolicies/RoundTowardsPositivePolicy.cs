namespace Archetypes.Quantity.RoundingPolicies;

public sealed class RoundTowardsPositivePolicy : IRoundPolicy
{
    public byte Precision { get; }
    public RoundTowardsPositivePolicy(byte precision) => Precision = precision;

    public decimal Round(decimal amount) => Math.Round(amount, Precision, MidpointRounding.ToPositiveInfinity);
}