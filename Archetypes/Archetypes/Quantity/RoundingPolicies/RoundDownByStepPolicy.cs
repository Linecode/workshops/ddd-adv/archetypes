namespace Archetypes.Quantity.RoundingPolicies;

public sealed class RoundDownByStepPolicy : IRoundPolicy
{
    public decimal RoundingStep { get; }

    public RoundDownByStepPolicy(decimal roundingStep) => RoundingStep = roundingStep;

    public decimal Round(decimal amount)
    {
        int amountSign = Math.Sign(amount);
        decimal absoluteAmount = Math.Abs(amount);
        decimal multiplicator = decimal.Floor(absoluteAmount / RoundingStep);

        return amountSign switch
        {
            1 => multiplicator * RoundingStep,
            0 => 0,
            -1 => multiplicator * RoundingStep * -1,
            _ => throw new ArgumentOutOfRangeException()
        };
    }
}