namespace Archetypes.Quantity.RoundingPolicies;

public sealed class RoundUpPolicy : IRoundPolicy
{
    public byte Precision { get; }
    public RoundUpPolicy(byte precision) => Precision = precision;

    public decimal Round(decimal amount)
        => amount switch
        {
            > decimal.Zero => Math.Round(amount, Precision, MidpointRounding.ToPositiveInfinity),
            < decimal.Zero => Math.Round(amount, Precision, MidpointRounding.ToNegativeInfinity),
            _ => amount
        };
}