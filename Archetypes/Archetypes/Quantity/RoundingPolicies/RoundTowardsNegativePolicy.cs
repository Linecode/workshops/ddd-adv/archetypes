namespace Archetypes.Quantity.RoundingPolicies;

public sealed class RoundTowardsNegativePolicy : IRoundPolicy
{
    public byte Precision { get; }
    public RoundTowardsNegativePolicy(byte precision) => Precision = precision;

    public decimal Round(decimal amount) => Math.Round(amount, Precision, MidpointRounding.ToNegativeInfinity);
}