namespace Archetypes.Quantity;

public static class Logic
{
    public static void Run()
    {
        var distanceInMeters = new Quantity(1000, new Meter());
        
        Console.WriteLine(distanceInMeters.ConvertTo(new Kilometer()).Value);
    }
}