namespace Archetypes.Quantity;

public class Quantity
{
    public double Value { get; set; }
    public Unit Unit { get; set; }

    public Quantity(double value, Unit unit)
    {
        Value = value;
        Unit = unit;
    }

    public Quantity ConvertTo(Unit targetUnit)
    {
        double baseValue = Unit.ConvertToBaseUnit(Value);
        double targetValue = targetUnit.ConvertFromBaseUnit(baseValue);
        return new (targetValue, targetUnit);
    }
}