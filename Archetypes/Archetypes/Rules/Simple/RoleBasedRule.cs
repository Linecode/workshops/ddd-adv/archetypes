// namespace Archetypes.Rules.Simple;
//
// public class RoleBasedRule : IRule
// {
//     private readonly CustomerRole _requiredRole;
//
//     public RoleBasedRule(CustomerRole requiredRole)
//     {
//         _requiredRole = requiredRole;
//     }
//
//     public bool Evaluate(Customer customer)
//     {
//         return customer.Role == _requiredRole;
//     }
// }