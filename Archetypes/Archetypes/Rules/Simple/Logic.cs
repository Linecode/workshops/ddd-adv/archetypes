namespace Archetypes.Rules.Simple;

public static class Logic
{
    public static void Run()
    {
        var customer = new Customer
        {
            Id = 1,
            AccountBalance = 2000,
            CreditScore = 700,
            Role = CustomerRole.VIP,
            HasGoodHistory = true
        };
        
        // var ruleEngine = new RuleEngine();
        // ruleEngine.AddRule(new BalanceRule(1500));
        // ruleEngine.AddRule(new CreditScoreRule(650));
        // ruleEngine.AddRule(new RoleBasedRule(CustomerRole.VIP));
        // ruleEngine.AddRule(new HistoryRule());
        //
        // bool isEligible = ruleEngine.EvaluateRules(customer);
        // Console.WriteLine($"Customer Eligibility: {isEligible}");
        try
        {
            customer.PlaceOrder(new Customer.Command.PlaceOrder(new DateTimeProvider(), new List<object>()));
            
            Console.WriteLine($"Customer Is able to place order");
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
}