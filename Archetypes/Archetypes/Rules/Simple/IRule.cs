namespace Archetypes.Rules.Simple;

// public interface IRule
// {
//     bool Evaluate(Customer customer);
// }

public interface IRule<T> where T : Aggregate<T>
{
    bool Evaluate(T aggregate);
}