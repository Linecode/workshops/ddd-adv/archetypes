// namespace Archetypes.Rules.Simple;
//
// public class BalanceRule : IRule
// {
//     private readonly double _requiredBalance;
//
//     public BalanceRule(double requiredBalance)
//     {
//         _requiredBalance = requiredBalance;
//     }
//
//     public bool Evaluate(Customer customer)
//     {
//         return customer.AccountBalance >= _requiredBalance;
//     }
// }