namespace Archetypes.Rules.Simple;

public enum CustomerRole
{
    Regular,
    VIP,
    Student
}