// namespace Archetypes.Rules.Simple;
//
// public class CreditScoreRule : IRule
// {
//     private readonly int _requiredScore;
//
//     public CreditScoreRule(int requiredScore)
//     {
//         _requiredScore = requiredScore;
//     }
//
//     public bool Evaluate(Customer customer)
//     {
//         return customer.CreditScore >= _requiredScore;
//     }
// }