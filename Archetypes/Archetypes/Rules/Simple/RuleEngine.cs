namespace Archetypes.Rules.Simple;

// public class RuleEngine
// {
//     private readonly List<IRule> _rules = new();
//
//     public void AddRule(IRule rule)
//     {
//         _rules.Add(rule);
//     }
//
//     public bool EvaluateRules(Customer customer)
//     {
//         foreach (var rule in _rules)
//         {
//             if (!rule.Evaluate(customer))
//             {
//                 return false;
//             }
//         }
//
//         return true;
//     }
// }

public class RuleEngine<T> where T : Aggregate<T>
{
    private readonly List<IRule<T>> _rules = new();
    
    public void AddRule(IRule<T> rule)
    {
        _rules.Add(rule);
    }
    
    // We can return broken rules list for example if we need to communicate which rules are broken
    public bool EvaluateRules(T aggregate)
    {
        foreach (var rule in _rules)
        {
            if (!rule.Evaluate(aggregate))
            {
                return false;
            }
        }
        return true;
    }
}