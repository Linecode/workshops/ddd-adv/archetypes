namespace Archetypes.Rules.Simple;

public class Customer : Aggregate<Customer>
{
    public int Id { get; set; }
    public double AccountBalance { get; set; }
    public int CreditScore { get; set; }
    public CustomerRole Role { get; set; }
    public bool HasGoodHistory { get; set; }
    public DateTime CreateAt { get; set; }

    public void PlaceOrder(Command.PlaceOrder command)
    {
        AddRule(new DateCanNotBeInFutureRule(command.DateTimeProvider));
        AddRule(new AnotherRule());
        
        if (!EvaluateRules())
        {
            throw new InvalidOperationException();
        }
    }

    public abstract record Command
    {
        public record PlaceOrder(IDateTimeProvider DateTimeProvider, List<object> Lines) : Command;
    }
}

public class DateCanNotBeInFutureRule : IRule<Customer>
{
    private readonly IDateTimeProvider _dateTimeProvider;

    public DateCanNotBeInFutureRule(IDateTimeProvider dateTimeProvider)
    {
        _dateTimeProvider = dateTimeProvider;
    }
    
    public bool Evaluate(Customer aggregate)
    {
        return _dateTimeProvider.Now >= aggregate.CreateAt;
    }
}

public class AnotherRule : IRule<Customer>
{
    public bool Evaluate(Customer aggregate)
    {
        return true;
    }
}

public abstract class Aggregate<T> where T : Aggregate<T>
{
    private readonly RuleEngine<T> _ruleEngine = new();
    
    protected void AddRule(IRule<T> rule) => _ruleEngine.AddRule(rule);

    protected bool EvaluateRules()
    {
        return _ruleEngine.EvaluateRules(this as T ?? throw new ArgumentNullException());
    }
}

public interface IDateTimeProvider
{
    DateTime Now { get; }
}

public class DateTimeProvider : IDateTimeProvider
{
    public DateTime Now => DateTime.Now;
}