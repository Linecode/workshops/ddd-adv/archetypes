namespace Archetypes.Product.WithOrder;

public enum OrderStatus { Initializing, Open, Closed, Cancelled }