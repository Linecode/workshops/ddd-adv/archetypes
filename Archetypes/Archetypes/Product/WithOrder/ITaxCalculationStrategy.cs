namespace Archetypes.Product.WithOrder;

public interface ITaxCalculationStrategy
{
    decimal CalculateTax(decimal subtotal);
}

public class StandardTaxCalculation : ITaxCalculationStrategy
{
    public decimal CalculateTax(decimal subtotal)
    {
        const decimal taxRate = 0.1m; // Example 10% tax rate
        return subtotal * taxRate;
    }
}