namespace Archetypes.Product.WithOrder;

public class OpenEvent : LifecycleEvent
{
    public string SalesChannel { get; private set; }

    public OpenEvent(Guid orderId, string salesChannel) : base(orderId, "Order opened")
    {
        SalesChannel = salesChannel;
    }
}

public class CloseEvent : LifecycleEvent
{
    public CloseEvent(Guid orderId) : base(orderId, "Order closed") { }
}

public class CancelEvent : LifecycleEvent
{
    public CancelEvent(Guid orderId) : base(orderId, "Order cancelled") { }
}

public class DiscountEvent : OrderEvent
{
    public decimal DiscountAmount { get; private set; }

    public DiscountEvent(Guid orderId, decimal discountAmount) 
        : base(orderId, $"Discount applied: {discountAmount}")
    {
        DiscountAmount = discountAmount;
    }
}