namespace Archetypes.Product.WithOrder;

public abstract class OrderEvent
{
    public DateTime EventDateTime { get; protected set; }
    public string EventDescription { get; protected set; }
    public Guid OrderId { get; protected set; }

    protected OrderEvent(Guid orderId, string description)
    {
        OrderId = orderId;
        EventDateTime = DateTime.Now;
        EventDescription = description;
    }
}

public abstract class LifecycleEvent : OrderEvent
{
    protected LifecycleEvent(Guid orderId, string description) : base(orderId, description)
    {
    }
}