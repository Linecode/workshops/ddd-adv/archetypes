using Archetypes.Product.FeatureV2;

namespace Archetypes.Product.WithOrder;

public class OrderLine
{
    public CatalogEntry Product { get; private set; }
    public int Quantity { get; private set; }
    public decimal LineTotal => Product.Product.CalculateTotalCost() * Quantity;

    public OrderLine(CatalogEntry product, int quantity)
    {
        Product = product;
        Quantity = quantity;
    }
    
    public override string ToString()
    {
        return $"{Product.Product.GetType().Name} x {Quantity} ({LineTotal} net)";
    }
}