using System.Text;
using Archetypes.Product.FeatureV2;

namespace Archetypes.Product.WithOrder;

public class Order
{
    public Guid OrderId { get; private set; }

    private readonly List<OrderLine> _lines = new();
    public IReadOnlyList<OrderLine> Lines => _lines;
    
    private readonly List<OrderEvent> _events = new();
    public IReadOnlyList<OrderEvent> Events => _events;
    
    public string SalesChannel { get; private set; }
    public OrderStatus Status { get; private set; } = OrderStatus.Initializing;
    private readonly ITaxCalculationStrategy _taxCalculationStrategy;

    public Order(ITaxCalculationStrategy taxCalculationStrategy, string salesChannel = "Online")
    {
        OrderId = Guid.NewGuid();
        _taxCalculationStrategy = taxCalculationStrategy;
        SalesChannel = salesChannel;
    }
    
    private void UpdateStatus(OrderStatus newStatus)
    {
        Status = newStatus;
    }

    public void AddOrderLine(CatalogEntry product, int quantity)
    {
        _lines.Add(new OrderLine(product, quantity));
    }

    public void ProcessDiscountEvent(DiscountEvent @event)
    {
        _events.Add(@event);
    }

    public void ProcessLifecycleEvent(OrderEvent @event)
    {
        Handle((dynamic)@event);
    }

    private void Handle(OpenEvent @event)
    {
        _events.Add(@event);
        Status = OrderStatus.Open;
    }
    
    private void Handle(CloseEvent @event)
    {
        _events.Add(@event);
        Status = OrderStatus.Closed;
    }
    
    private void Handle(CancelEvent @event)
    {
        _events.Add(@event);
        Status = OrderStatus.Cancelled;
    }
    
    public decimal CalculateTotalCost()
    {
        decimal subtotal = 0m;
        foreach (var line in Lines)
        {
            subtotal += line.LineTotal;
        }
        
        // Discounts
        foreach (var @event in _events.Where(x => x is DiscountEvent).Cast<DiscountEvent>())
        {
            subtotal -= @event.DiscountAmount;
        }
        
        return subtotal + _taxCalculationStrategy.CalculateTax(subtotal);
    }

    public string DisplayOrderDetails()
    {
        var sb = new StringBuilder();
        sb.AppendLine($"Order Id: {OrderId}");
        sb.AppendLine($"Order Status: {Status}");
        sb.AppendLine($"Order Total: ${CalculateTotalCost()}");
        sb.AppendLine("Order Lines: ");

        foreach (var line in Lines)
        {
            sb.AppendLine(line.ToString());
        }
        
        return sb.ToString();
    }
}