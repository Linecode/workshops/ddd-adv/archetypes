namespace Archetypes.Product.WithOrder;

// OrderManager Archetype
public class OrderFacade
{
    public void AddOrder(Order order)
    {
        // Save order in repository
    }
    
    public void ProcessOrder(Order order)
    {
        order.ProcessLifecycleEvent(new OpenEvent(order.OrderId, order.SalesChannel));
    }

    public void FinalizeOrder(Order order)
    {
        order.ProcessLifecycleEvent(new CloseEvent(order.OrderId));
    }

    public void CancelOrder(Order order)
    {
        order.ProcessLifecycleEvent(new CancelEvent(order.OrderId));
    }

    public void AddDiscount(Order order /*,  DiscountType type*/)
    {
        order.ProcessDiscountEvent(new DiscountEvent(order.OrderId, 100m));
    }

    public Order FindOrderById(Guid orderId)
    {
        return null;
    }
}