using Archetypes.Product.FeatureV2;

namespace Archetypes.Product.WithOrder;

public static class Logic
{
    public static void Run()
    {
        // Create a tax calculation strategy
        ITaxCalculationStrategy taxStrategy = new StandardTaxCalculation();

        // Create a new order with the tax calculation strategy
        Order myOrder = new Order(taxStrategy);

        // Simulate adding products (CatalogEntries) to the order
        var basicComputer = new Computer();
        basicComputer.AddFeature(new ProcessorFeature("Intel Core i5", 200m));
        basicComputer.AddFeature(new RAMFeature(8, 80m));
        var basicEntry = new CatalogEntry("Basic Computer", basicComputer);

        var advancedComputer = new Computer();
        advancedComputer.AddFeature(new ProcessorFeature("Intel Core i7", 300m));
        advancedComputer.AddFeature(new RAMFeature(16, 120m));
        var advancedEntry = new CatalogEntry("Advanced Computer", advancedComputer);

        // Add order lines to the order
        myOrder.AddOrderLine(basicEntry, 2); // 2 Basic Computers
        myOrder.AddOrderLine(advancedEntry, 1); // 1 Advanced Computer

        // Handle order events

        // Process and finalize the order
        OrderFacade orderFacade = new OrderFacade();
        orderFacade.ProcessOrder(myOrder);
        orderFacade.AddDiscount(myOrder);
        orderFacade.FinalizeOrder(myOrder);

        // Display order details
        Console.WriteLine(myOrder.DisplayOrderDetails());
        Console.WriteLine($"Total Order Cost: ${myOrder.CalculateTotalCost()}");
    }
}