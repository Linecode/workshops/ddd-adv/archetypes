using System.Text;

namespace Archetypes.Product.FeatureV2;

public class ProductCatalog
{
    private List<CatalogEntry> entries = new();

    public void AddEntry(CatalogEntry entry)
    {
        entries.Add(entry);
    }

    public string DisplayCatalog()
    {
        StringBuilder catalogDisplay = new StringBuilder("Product Catalog:\n");
        foreach (var entry in entries)
        {
            catalogDisplay.AppendLine(entry.GetCatalogEntryDetails());
            catalogDisplay.AppendLine("--------------------------------------");
        }
        return catalogDisplay.ToString();
    }
}