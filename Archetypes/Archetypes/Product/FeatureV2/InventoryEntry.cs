namespace Archetypes.Product.FeatureV2;

public class InventoryEntry
{
    public CatalogEntry ProductEntry { get; private set; }
    public int QuantityInStock { get; private set; }

    public InventoryEntry(CatalogEntry productEntry, int initialStock)
    {
        ProductEntry = productEntry;
        QuantityInStock = initialStock;
    }

    public void UpdateStock(int changeInQuantity)
    {
        QuantityInStock += changeInQuantity;
    }

    public string GetInventoryDetails()
    {
        return $"Product: {ProductEntry.EntryName}, Stock: {QuantityInStock}";
    }
}