namespace Archetypes.Product.FeatureV2;

public class ProcessorFeature : IProductFeature
{
    public string ProcessorType { get; private set; }
    public decimal Cost { get; private set; }

    public ProcessorFeature(string processorType, decimal cost)
    {
        ProcessorType = processorType;
        Cost = cost;
    }

    public void ApplyFeature(Computer computer)
    {
        computer.Processor = ProcessorType;
    }

    public string GetFeatureDetails()
    {
        return $"Processor: {ProcessorType}";
    }

    public decimal GetCost()
    {
        return Cost;
    }
}