using System.Text;

namespace Archetypes.Product.FeatureV2;

public class CatalogEntry
{
    public Computer Product { get; private set; }
    public string EntryName { get; private set; }

    public CatalogEntry(string entryName, Computer product)
    {
        EntryName = entryName;
        Product = product;
    }

    public string GetCatalogEntryDetails()
    {
        var details = new StringBuilder($"Catalog Entry: {EntryName}\n");
        details.AppendLine(Product.GetComputerSpecs());
        details.AppendLine($"Total Cost: ${Product.CalculateTotalCost()}");
        return details.ToString();
    }
}