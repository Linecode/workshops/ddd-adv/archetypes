namespace Archetypes.Product.FeatureV2;

public class Reservation
{
    public CatalogEntry ReservedProduct { get; private set; }
    public string CustomerName { get; private set; }

    public Reservation(string customerName, CatalogEntry reservedProduct)
    {
        CustomerName = customerName;
        ReservedProduct = reservedProduct;
    }

    public bool ReserveProduct(Inventory inventory)
    {
        // Find the inventory entry for the reserved product
        foreach (var entry in inventory.Entries)
        {
            if (entry.ProductEntry == ReservedProduct && entry.QuantityInStock > 0)
            {
                entry.UpdateStock(-1); // Reduce the stock by 1
                return true; // Reservation successful
            }
        }
        return false; // Reservation unsuccessful due to no stock
    }

    public string GetReservationDetails()
    {
        return $"Reservation for {CustomerName}: {ReservedProduct.EntryName}";
    }
}