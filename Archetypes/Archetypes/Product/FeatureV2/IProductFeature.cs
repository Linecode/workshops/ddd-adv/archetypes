namespace Archetypes.Product.FeatureV2;

// Updated ProductFeature Archetype Interface with Cost
public interface IProductFeature
{
    void ApplyFeature(Computer computer);
    string GetFeatureDetails();
    decimal GetCost();
}