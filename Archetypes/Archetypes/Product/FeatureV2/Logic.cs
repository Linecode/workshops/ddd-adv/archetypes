using System.Text;

namespace Archetypes.Product.FeatureV2;

public static class Logic
{
    public static void Run()
    {
        var catalog = new ProductCatalog();
        var inventory = new Inventory();

        // Create and add catalog entries and inventory entries
        var basicComputer = new Computer();
        basicComputer.AddFeature(new ProcessorFeature("Intel Core i5", 200m));
        basicComputer.AddFeature(new RAMFeature(8, 80m));
        var basicEntry = new CatalogEntry("Basic Computer", basicComputer);
        catalog.AddEntry(basicEntry);
        inventory.AddEntry(new InventoryEntry(basicEntry, 10)); // 10 units in stock

        var advancedComputer = new Computer();
        advancedComputer.AddFeature(new ProcessorFeature("Intel Core i7", 300m));
        advancedComputer.AddFeature(new RAMFeature(16, 120m));
        var advancedEntry = new CatalogEntry("Advanced Computer", advancedComputer);
        catalog.AddEntry(advancedEntry);
        inventory.AddEntry(new InventoryEntry(advancedEntry, 5)); // 5 units in stock

        // Reservation
        var reservation = new Reservation("John Doe", advancedEntry);
        var isReserved = reservation.ReserveProduct(inventory);
        Console.WriteLine(isReserved ? "Reservation successful." : "Reservation failed.");
        Console.WriteLine(reservation.GetReservationDetails());

        Console.WriteLine(inventory.DisplayInventory());
    }
}