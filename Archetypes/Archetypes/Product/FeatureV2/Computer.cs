using System.Text;

namespace Archetypes.Product.FeatureV2;

public class Computer
{
    public string Processor { get; set; }
    public int RAM { get; set; }
    
    // ... Other hardware properties ...

    public List<IProductFeature> Features { get; private set; } = new();

    public void AddFeature(IProductFeature feature)
    {
        Features.Add(feature);
        feature.ApplyFeature(this);
    }

    public string GetComputerSpecs()
    {
        StringBuilder specs = new StringBuilder("Computer Specifications:\n");
        foreach (var feature in Features)
        {
            specs.AppendLine(feature.GetFeatureDetails());
        }
        return specs.ToString();
    }

    public decimal CalculateTotalCost()
    {
        return Features.Sum(feature => feature.GetCost());
    }
}