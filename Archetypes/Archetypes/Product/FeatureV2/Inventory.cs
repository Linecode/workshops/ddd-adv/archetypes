using System.Text;

namespace Archetypes.Product.FeatureV2;

public class Inventory
{
    private readonly List<InventoryEntry> _entries = new();
    public IReadOnlyList<InventoryEntry> Entries => _entries.AsReadOnly();

    public void AddEntry(InventoryEntry entry)
    {
        _entries.Add(entry);
    }

    public string DisplayInventory()
    {
        var inventoryDisplay = new StringBuilder("Inventory:\n");
        foreach (var entry in _entries)
        {
            inventoryDisplay.AppendLine(entry.GetInventoryDetails());
        }
        return inventoryDisplay.ToString();
    }
}