namespace Archetypes.Product.FeatureV2;

public class RAMFeature : IProductFeature
{
    public int RAMSize { get; private set; }
    public decimal Cost { get; private set; }

    public RAMFeature(int ramSize, decimal cost)
    {
        RAMSize = ramSize;
        Cost = cost;
    }

    public void ApplyFeature(Computer computer)
    {
        computer.RAM = RAMSize;
    }

    public string GetFeatureDetails()
    {
        return $"RAM: {RAMSize}GB";
    }

    public decimal GetCost()
    {
        return Cost;
    }
}