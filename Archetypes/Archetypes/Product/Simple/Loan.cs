namespace Archetypes.Product.Simple;

public abstract class Loan : ILoan
{
    public string Name { get; protected set; }
    public string Description { get; protected set; }
    public Guid Id { get; protected set; }
    
    public decimal Principal { get; protected set; }
    public float InterestRate { get; protected set; } // Annual interest rate

    protected Loan(decimal principal, float interestRate)
    {
        Principal = principal;
        InterestRate = interestRate;
    }

    public abstract decimal CalculateInterest();
}