namespace Archetypes.Product.Simple;

// Product Type
public interface ILoan
{
    decimal CalculateInterest();
}