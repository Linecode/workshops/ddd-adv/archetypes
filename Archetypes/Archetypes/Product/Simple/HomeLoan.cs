namespace Archetypes.Product.Simple;

// Product Instance
public class HomeLoan : Loan
{
    public HomeLoan(decimal principal, float interestRate)
        : base(principal, interestRate)
    {
        Id = Guid.NewGuid();
        Name = "Home Loan";
        Description = "Home Loan";
    }

    public override decimal CalculateInterest()
    {
        // Different interest calculation for home loans
        return Principal * (decimal)(InterestRate / 100) * 0.9m; // Example with a discount factor
    }
}