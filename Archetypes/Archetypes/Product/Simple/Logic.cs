namespace Archetypes.Product.Simple;

public static class Logic
{
    public static void Run()
    {
        ILoan personalLoan = new PersonalLoan(10000m, 5f);
        Console.WriteLine($"Interest: {personalLoan.CalculateInterest()}");

        ILoan homeLoan = new HomeLoan(50000m, 3.5f);
        Console.WriteLine($"Interest: {homeLoan.CalculateInterest()}");
    }
}