namespace Archetypes.Product.Simple;

// Product Instance
public class PersonalLoan : Loan
{
    public PersonalLoan(decimal principal, float interestRate) : base(principal, interestRate)
    {
        Name = "Personal Loan";
        Description = "Personal Loan";
        Id = Guid.NewGuid();
    }

    public override decimal CalculateInterest()
    {
        // Simple interest calculation for demonstration
        return Principal * (decimal)(InterestRate / 100);
    }
}