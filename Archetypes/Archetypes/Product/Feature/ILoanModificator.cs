namespace Archetypes.Product.Feature;

// Product Feature Type
// Why Modificator not feature? cause our business is using that term
public interface ILoanModificator
{
    decimal ModifyInterest(decimal interest);
}

public interface ILoanMonthsModificator : ILoanModificator
{
    int ModifyMonths(int months);
}

public class RuralLoanModificator : ILoanModificator
{
    public decimal ModifyInterest(decimal interest)
    {
        return interest * 0.9m; // Example with a discount factor
    }
}

public class OneYearExtensionModificator : ILoanMonthsModificator
{
    public int ModifyMonths(int months)
    {
        return months + 12;
    }

    public decimal ModifyInterest(decimal interest)
    {
        return interest * 1.1m; // Example with a discount factor
    }
}

public class TwoYearsExtensionModificator : ILoanMonthsModificator
{
    public int ModifyMonths(int months)
    {
        return months + 24;
    }

    public decimal ModifyInterest(decimal interest)
    {
        return interest * 1.2m; // Example with a discount factor
    }
}