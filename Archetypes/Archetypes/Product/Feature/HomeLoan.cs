namespace Archetypes.Product.Feature;

// Product Instance
public class HomeLoan : Loan
{
    public HomeLoan(decimal principal, float interestRate)
        : base(principal, interestRate)
    {
        Id = Guid.NewGuid();
        Name = "Home Loan";
        Description = "Home Loan";
        Months = 120;
    }

    public override decimal CalculateBaseInterest()
    {
        // Different interest calculation for home loans
        return Principal * (decimal)(InterestRate / 100) * 0.9m; // Example with a discount factor
    }
}