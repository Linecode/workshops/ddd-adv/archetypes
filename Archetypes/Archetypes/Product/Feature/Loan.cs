namespace Archetypes.Product.Feature;

public abstract class Loan : ILoan
{
    public string Name { get; protected set; }
    public string Description { get; protected set; }
    public Guid Id { get; protected set; }
    
    public decimal Principal { get; protected set; }
    public int Months { get; protected set; }
    public float InterestRate { get; protected set; } // Annual interest rate

    protected List<ILoanModificator> _modificators = new();

    protected Loan(decimal principal, float interestRate)
    {
        Principal = principal;
        InterestRate = interestRate;
    }

    public void AddLoanModificator(ILoanModificator modificator)
    {
        _modificators.Add(modificator);
    }

    public abstract decimal CalculateBaseInterest();

    public decimal CalculateTotalInterest()
    {
        var interest = CalculateBaseInterest();
        
        foreach (var modificator in _modificators)
        {
            interest = modificator.ModifyInterest(interest);
        }
        
        return interest;
    }

    public int CalculateMonths()
    {
        return _modificators.Where(x => x is ILoanMonthsModificator).ToList().Cast<ILoanMonthsModificator>()
            .Aggregate(Months, (current, modificator) => modificator.ModifyMonths(current));
    }
}