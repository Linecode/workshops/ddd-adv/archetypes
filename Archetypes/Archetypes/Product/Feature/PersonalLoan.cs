namespace Archetypes.Product.Feature;

// Product Instance
public class PersonalLoan : Loan
{
    public PersonalLoan(decimal principal, float interestRate) : base(principal, interestRate)
    {
        Name = "Personal Loan";
        Description = "Personal Loan";
        Id = Guid.NewGuid();
        Months = 60;
    }

    public override decimal CalculateBaseInterest()
    {
        // Simple interest calculation for demonstration
        return Principal * (decimal)(InterestRate / 100);
    }
}