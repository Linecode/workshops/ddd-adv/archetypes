namespace Archetypes.Product.Feature;

// Product Type
public interface ILoan
{
    decimal CalculateTotalInterest();
}