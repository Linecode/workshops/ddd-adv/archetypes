namespace Archetypes.Product.Feature;

public static class Logic
{
    public static void Run()
    {
        Loan personalLoan = new PersonalLoan(10000m, 5f);
        Console.WriteLine($"Interest: {personalLoan.CalculateTotalInterest()} Months: {personalLoan.CalculateMonths()}");
        
        Loan homeLoanV1 = new HomeLoan(50000m, 3.5f);
        Console.WriteLine($"Interest: {homeLoanV1.CalculateTotalInterest()} Months: {homeLoanV1.CalculateMonths()}");

        Loan homeLoanV2 = new HomeLoan(50000m, 3.5f);
        homeLoanV2.AddLoanModificator(new RuralLoanModificator());
        Console.WriteLine($"Interest: {homeLoanV2.CalculateTotalInterest()} Months: {homeLoanV2.CalculateMonths()}");
        
        Loan homeLoanV3 = new HomeLoan(50000m, 3.5f);
        homeLoanV3.AddLoanModificator(new OneYearExtensionModificator());
        Console.WriteLine($"Interest: {homeLoanV3.CalculateTotalInterest()} Months: {homeLoanV3.CalculateMonths()}");
    }
}