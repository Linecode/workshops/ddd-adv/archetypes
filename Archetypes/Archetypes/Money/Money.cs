namespace Archetypes.Money;

public class Money
{
    public decimal Amount { get; }
    public Currency Currency { get; }

    public Money(decimal amount, Currency currency)
    {
        Amount = amount;
        Currency = currency;
    }

    public Money Add(Money other)
    {
        if (Currency != other.Currency)
            throw new InvalidOperationException("Cannot add amounts in different currencies");

        return new (Amount + other.Amount, Currency);
    }

    // Additional methods like Subtract, Multiply etc.
}