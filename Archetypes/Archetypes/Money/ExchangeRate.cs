namespace Archetypes.Money;

public class ExchangeRate
{
    public Currency FromCurrency { get; }
    public Currency ToCurrency { get; }
    public decimal Rate { get; }

    public ExchangeRate(Currency fromCurrency, Currency toCurrency, decimal rate)
    {
        FromCurrency = fromCurrency;
        ToCurrency = toCurrency;
        Rate = rate;
    }

    public Money Convert(Money fromMoney)
    {
        if (fromMoney.Currency != FromCurrency)
            throw new InvalidOperationException("Currency mismatch");

        var convertedAmount = fromMoney.Amount * Rate;
        return new (convertedAmount, ToCurrency);
    }
}