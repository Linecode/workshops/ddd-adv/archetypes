namespace Archetypes.Money;

public static class Logic
{
    public static void Run()
    {
        // Creating Currency instances
        var usd = new Currency("USD");
        var eur = new Currency("EUR");

        // Creating Money instances in USD and EUR
        var wallet1 = new Money(100m, usd); // $100
        var wallet2 = new Money(50m, usd);  // $50
        var wallet3 = new Money(80m, eur);  // €80
        
        // Adding money (same currency)
        var totalUsd = wallet1.Add(wallet2); // Should be $150

        // Trying to add different currencies (should throw exception)
        try
        {
            var invalidTotal = wallet1.Add(wallet3); // This should fail
        }
        catch (InvalidOperationException ex)
        {
            Console.WriteLine(ex.Message); // Outputs error message
        }
        
        // Creating an Exchange Rate (1 USD = 0.85 EUR)
        var usdToEurRate = new ExchangeRate(usd, eur, 0.85m);

        // Converting $100 to €
        var convertedWallet = usdToEurRate.Convert(wallet1); // Should be €85
        
        Console.WriteLine($"Total in USD wallet: {totalUsd.Amount} {totalUsd.Currency.Code}");
        Console.WriteLine($"Converted to EUR: {convertedWallet.Amount} {convertedWallet.Currency.Code}");
    }
}