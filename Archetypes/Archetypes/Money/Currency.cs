namespace Archetypes.Money;

public class Currency
{
    public string Code { get; } // e.g., "USD", "EUR"

    public Currency(string code)
    {
        Code = code;
    }

    // Additional properties and methods
}